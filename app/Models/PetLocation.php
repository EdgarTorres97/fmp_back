<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PetLocation extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_municipality',
        'plManzana',
        'plRegion',
        'plLote',
        'plPostalCode',
        'plLostDate',
        'pllatitude',
        'pllongitude',
    ];
    public function municipalitys() {
        return $this->belongsToMany(Municipality::class,'id');
    }
}
