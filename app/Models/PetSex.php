<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PetSex extends Model
{
    use HasFactory;
    protected $fillable = [
        'psName',
        'psCounter'
    ];
}
