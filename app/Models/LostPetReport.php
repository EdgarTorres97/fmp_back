<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LostPetReport extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_user',
        'id_petType',
        'id_petSex',
        'id_petChip',
        'id_petTag',
        'id_petLocation',
        'lprPetName',
        'lprPhoneNumber',
        'lprBreedName',
        'lprSize',
        'lprAge',
        'lprTail',
        'lprEar',
        'lprColor',
        'lprObservation',
        'id_petProcess'
    ];


    public function users() {
        return $this->belongsToMany(User::class,'id');
    }
    public function petTypes() {
        return $this->belongsToMany(PetType::class,'id');
    }
    public function petSexs() {
        return $this->belongsToMany(PetSex::class,'id');
    }
    public function petChips() {
        return $this->belongsToMany(PetChip::class,'id');
    }
    public function petTags() {
        return $this->belongsToMany(PetTag::class,'id');
    }
    public function petLocations() {
        return $this->belongsToMany(PetLocation::class,'id');
    }
    public function petProcesses() {
        return $this->belongsToMany(PetLocation::class,'id');
    }
}
