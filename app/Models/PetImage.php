<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PetImage extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_lostPetReport',
        'piImageUrl',
    ];
    public function lostPetReport() {
        return $this->belongsToMany(LostPetReport::class,'id');
    }
}
