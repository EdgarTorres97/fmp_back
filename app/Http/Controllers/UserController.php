<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use DB;
class UserController extends Controller
{
    public function validarToken(){
        return response()->json([
            'token' => true
        ],200);  
    }
    //Registrar usuario
    public function register(Request $request){
        try{ 

            $role = '3';
            $validName = false;
            $validEmail = false;
            $validPhoneNumber = false;
            $validAge = false;
            $validPassword = false;
            $name = '';
            $email = '';
            $phoneNumber = '';
            $age = '';
            $password = '';

            if($request->has('name')) {
                if($request->input('name')==""){
                    return response()->json([
                        'error' => "El nombre no es valido"
                    ]);
                    
                }
                
                $this -> validate($request,[
                    'name'=>'required',
                ]);
                $validName = true;
                
            }else{
                return response()->json([
                    'error' => "El nombre es requerido"
                ]); 
                
            }
            if($request->has('email')) {
                if($request->input('email')==""){
                    
                    return response()->json([
                        'error' => "El correo no es valido"
                    ]);
                }
                
                $this -> validate($request,[
                    'email'=>'required|unique:users',
                ]);
                $validEmail = true;
                
            }else{
                return response()->json([
                    'error' => "El correo es requerido"
                ]); 
            }
            if($request->has('phoneNumber')) {
                if($request->input('phoneNumber')==""){
                    return response()->json([
                        'error' => "El número de phoneNumber no es valido"
                    ]);
                }
                $this -> validate($request,[
                    'phoneNumber' => 'required',
                ]);
                $validPhoneNumber = true;
            }else{
                return response()->json([
                    'error' => "El número de phoneNumber es requerido"
                ]); 
            }
            if($request->has('age')) {
                if($request->input('age')==""){
                    return response()->json([
                        'error' => "La edad no es valida"
                    ]);
                }
                $this -> validate($request,[
                    'age' => 'required',
                ]);
                $validAge = true;
            }else{
                return response()->json([
                    'error' => "La edad es requerida"
                ]); 
            }
            if($request->has('password')) {
                if($request->input('password')==""){
                    return response()->json([
                        'error' => "La clave no es valida"
                    ]);
                }
                $this -> validate($request,[
                    'password'=>'required|min:6'
                ]);
                $validPassword = true;
            }else{
                return response()->json([
                    'error' => "La clave es requerida"
                ]); 
            }
            if($validName&&$validEmail&&$validPhoneNumber&&$validAge&&$validPassword){

                $user = User::create([
                    'name'=> addslashes($request->name),
                    'email'=>addslashes($request->email),
                    'phoneNumber'=> addslashes($request->phoneNumber),
                    'age' => addslashes($request->age),
                    'id_role' => addslashes($role),
                    'password'=>bcrypt($request->password),
                    'state'=>1,
                    'urlImage'=>'https://fmpback.ryumicon.com/public/images/users/findmypet-2zfotoGBkG9k1-fotoperfil.svg'
                ]);
                $token = $user->createToken('FindMyPetToken')->accessToken;
                return response()->json([
                'token'  => $token,
                'id'     => $user->id,
                'email'  => $request->email,
                'name'   => $request->name,
                'phoneNumber'=> $request->phoneNumber,
                'age' => $request->age,
                'id_role'    => $role,
                'state'=>1,
                'urlImage'=>'https://fmpback.ryumicon.com/public/images/users/findmypet-2zfotoGBkG9k1-fotoperfil.svg'
            ],200);
            }else{
                return response()->json([
                      'error' => "Usuario no valido"
                  ]);   
              }
            
            
            //return "hola";
        } catch(Throwable $e) {
            return response()->json([
                'error' => $e
            ]);
        }
    }
    //Iniciar sesion usuario
    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if(auth()->attempt($credentials)){
            $token = auth()->user()->createToken('LicenciaToken')->accessToken;
            //$imagen = 'http://localhost:8002/';
            $imagen = auth()->user()->urlImage;
            return response()->json(['token' => $token,
            "id"    => auth()->user()->id,
            "email" => auth()->user()->email,
            "name"  => auth()->user()->name,
            "phoneNumber"  => auth()->user()->phoneNumber,
            "id_role"   => auth()->user()->id_role,
            "age"   => auth()->user()->age,
            "urlImage"=>$imagen

        ], 200);

        }
        else {
            return response()->json(['error' => 'UnAuthorised'], 401);
        }

    }
    public function modificarPerfil(Request $request){
        
        $id = auth()->user()->id;
        $consulta = DB::table('users')->select('*')->where('users.id',$id);
        
       try{
        $validarName = false;
        $validarPhoneNumber = false;
        $validarClave = false;
        $validarAge = false;
        if($request->has('name') ) {
            if($request->input('name')==""){
                $validarName = false;
            }else{
                $validarName = true;
            }
        }
        if($request->has('phoneNumber')){
            if($request->input('phoneNumber')==""){
                $validarPhoneNumber = false;
            }else{
                $validarPhoneNumber = true;
            }
        }
        if($request->has('age')){
            if($request->input('age')==""){
                $validarAge = false;
            }else{
                $validarAge = true;
            }
        }
        if($request->has('password')) {
            if($request->input('password')==""){
                $validarClave = false;
            }else{
                $validarClave = true;
            }
        }
        if($validarName){
            $consulta->update([
                'name'=>addslashes($request['name']),
            ]);
        }
        if($validarClave){
            $consulta->update([
                'password' => bcrypt($request['password'])
            ]);
        }
        if($validarPhoneNumber){
            $consulta->update([
                'phoneNumber'=> addslashes($request['phoneNumber'])
            ]);
        }
        if($validarAge){
            $consulta->update([
                'age'=> addslashes($request['age'])
            ]);
        }
        if($validarName || $validarPhoneNumber||$validarClave||$validarAge){
            return response()->json([
                'exito' => 'Exito'
            ]);
        }else{
            return response()->json([
                'error' => 'A ocurrido un error'
            ]);
        }
        
       } catch( Throwable $e){
        return response()->json([
            'error' => 'El campo está vacio'

        ]);
       }
    }
    public function usuarios() {
        $isAdmin = auth()->user()->id_role;

        try {
           if((strcmp( $isAdmin, 1) == 0 ) || strcmp($isAdmin, 2) == 0 ){
                
                $usuarios = User::where('id_role','3')->get();
                return response()->json($usuarios);
            }else{
                return response()->json([
                    "error" => "No tienes permiso"
                ]);
            }
        } catch(Throwable $e) {
            report($e);
            return response()->json([
                "error" => "A ocurrido un error"
            ]);
        }
    }
    public function administradores() {
        $isAdmin = auth()->user()->id_role;

        try {
            if(strcmp($isAdmin, 1)==0){
                $usuarios = User::where('id_role','1')->get();
                return response()->json($usuarios);
            }else{
                return response()->json([
                    "error" => "No tienes permiso"
                ]);
            }
        } catch(Throwable $e) {
            report($e);
            return response()->json([
                "error" => "A ocurrido un error"
            ]);
        }
    }
    public function colaboradores() {
        $isAdmin = auth()->user()->id_role;

        try {
            if(strcmp($isAdmin, 1)==0){
                $usuarios = User::where('id_role','2')->get();
                return response()->json($usuarios);
            }else{
                return response()->json([
                    "error" => "No tienes permiso"
                ]);
            }
        } catch(Throwable $e) {
            report($e);
            return response()->json([
                "error" => "A ocurrido un error"
            ],200);
        }
    }
    public function usuario(Request $request) {
        $isAdmin = auth()->user()->id_role;

        try {
            if(strcmp($isAdmin, 1)==0||strcmp($isAdmin, 2)==0) {
                if($request->has('id')){
                    $usuario = DB::table('users')->select(
                        'users.id',
                        'users.name',
                        'users.email',
                        'users.phoneNumber',
                        'users.age',
                        'users.urlImage',
                        'users.id_role',
                        
                        )->where('id',addslashes($request->input('id')));
                    $datos = $usuario->get()->toArray();
                    $json = json_decode(json_encode($datos),true);
                    if($json){
                        return response()->json($json);
                        
                    }
                    else {
                        return response()->json([
                            "error" => "No existe el usuario"
                        ]); 
        
                    }
                
                }else{
                    return response()->json([
                        "error" => "No existe el usuario"
                    ]);
                }
                
            }else{
                return response()->json([
                    "error" => "Acceso prohibido"
                ]);
            }
        } catch(Throwable $e) {
            report($e);
            return response()->json([
                "error" => $e
            ]);
        }
    }
    public function modificarUsuario(Request $request){
    
       try{
            $isAdmin = auth()->user()->id_role;
            if(strcmp($isAdmin, 1)==0||strcmp($isAdmin, 2)==0){

                if($request->has('id')){
                    $consulta = DB::table('users')->select('*')->where('users.id',addslashes($request->input('id')));
                    $validarName = false;
                    $validarPhoneNumber = false;
                    $validarClave = false;
                    $validarAge = false;
                    $validarId_role = false;
                    if($request->has('name') ) {
                        if($request->input('name')==""){
                            $validarName = false;
                        }else{
                            $validarName = true;
                        }
                    }
                    if($request->has('phoneNumber')){
                        if($request->input('phoneNumber')==""){
                            $validarPhoneNumber = false;
                        }else{
                            $validarPhoneNumber = true;
                        }
                    }
                    if($request->has('age')){
                        if($request->input('age')==""){
                            $validarAge = false;
                        }else{
                            $validarAge = true;
                        }
                    }
                    if($request->has('password')) {
                        if($request->input('password')==""){
                            $validarClave = false;
                        }else{
                            $validarClave = true;
                        }
                    }
                    if($request->has('id_role')) {
                        if($request->input('id_role')==""){
                            $validarId_role = false;
                        }else{
                            $validarId_role = true;
                        }
                    }
                    if($validarName){
                        $consulta->update([
                            'name'=>addslashes($request['name'],)
                        ]);
                    }
                    if($validarClave){
                        $consulta->update([
                            'password' => bcrypt($request['password'])
                        ]);
                    }
                    if($validarPhoneNumber){
                        $consulta->update([
                            'phoneNumber'=> addslashes($request['phoneNumber'])
                        ]);
                    }
                    if($validarAge){
                        $consulta->update([
                            'age'=> addslashes($request['age'])
                        ]);
                    }
                    if($validarId_role){
                        $consulta->update([
                            'id_role'=> addslashes($request['id_role'])
                        ]);
                    }
                    if($validarName || $validarPhoneNumber||$validarClave||$validarAge||$validarId_role){
                        return response()->json([
                            'exito' => 'Exito'
                        ]);
                    }else{
                        return response()->json([
                            'error' => 'A ocurrido un error'
                        ]);
                    }
                }else{
                    return response()->json([
                        'error' => 'El usuario no existe'
            
                    ]);
                }
                
            
            }else{
                return response()->json([
                    'error' => 'No tienes permiso'
        
                ]);
            }
        } catch( Throwable $e){
            return response()->json([
                'error' => 'El campo está vacio'

            ]);
        }
    }
    public function estatusUsuario(Request $request){
        try{
            $isAdmin = auth()->user()->id_role;
            if(strcmp($isAdmin, 1)==0||strcmp($isAdmin, 2)==0){

                if($request->has('id')&&$request->has('state')){
                    $consulta = DB::table('users')->select('*')->where('users.id',addslashes($request->input('id')));
                    
                    $consulta->update([
                        'users.state' => addslashes($request->input('state'))
                    ]);
                    return response()->json([
                        'exito' => 'El etatus se ha actualizado'
                    ]);
                }else{
                    return response()->json([
                        'error' => "Datos no validos"
            
                    ]);
                }
                
            
            }else{
                return response()->json([
                    'error' => 'No tienes permiso'
        
                ]);
            }
        } catch( Throwable $e){
            return response()->json([
                'error' => 'El campo está vacio'

            ]);
        }  
    }
    public function agregarUsuario(Request $request) {
        $isAdmin = auth()->user()->id_role;
            
        //   return $isAdmin;
        try {
            
            if(strcmp($isAdmin, 1)==0||strcmp($isAdmin, 2)==0){
                
            $validName = false;
            $validEmail = false;
            $validPhoneNumber = false;
            $validAge = false;
            $validPassword = false;
            $validRole = false;
            
            if($request->has('name')) {
                if($request->input('name')==""){
                    return response()->json([
                        'error' => "El nombre no es valido"
                    ]);
                    
                }
                
                $this -> validate($request,[
                    'name'=>'required',
                ]);
                $validName = true;
                
            }else{
                return response()->json([
                    'error' => "El nombre es requerido"
                ]); 
                
            }
            if($request->has('email')) {
                if($request->input('email')==""){
                    
                    return response()->json([
                        'error' => "El correo no es valido"
                    ]);
                }
                
                $this -> validate($request,[
                    'email'=>'required|unique:users',
                ]);
                $validEmail = true;
                
            }else{
                return response()->json([
                    'error' => "El correo es requerido"
                ]); 
            }
            if($request->has('phoneNumber')) {
                if($request->input('phoneNumber')==""){
                    return response()->json([
                        'error' => "El número de phoneNumber no es valido"
                    ]);
                }
                $this -> validate($request,[
                    'phoneNumber' => 'required',
                ]);
                $validPhoneNumber = true;
            }else{
                return response()->json([
                    'error' => "El número de phoneNumber es requerido"
                ]); 
            }
            if($request->has('age')) {
                if($request->input('age')==""){
                    return response()->json([
                        'error' => "La edad no es valida"
                    ]);
                }
                $this -> validate($request,[
                    'age' => 'required',
                ]);
                $validAge = true;
            }else{
                return response()->json([
                    'error' => "La edad es requerida"
                ]); 
            }
            if($request->has('password')) {
                if($request->input('password')==""){
                    return response()->json([
                        'error' => "La clave no es valida"
                    ]);
                }
                $this -> validate($request,[
                    'password'=>'required|min:6'
                ]);
                $validPassword = true;
            }else{
                return response()->json([
                    'error' => "La clave es requerida"
                ]); 
            }
            if($request->has('id_role')) {
                if($request->input('id_role')==""){
                    return response()->json([
                        'error' => "La rol no es valido"
                    ]);
                }
                $this -> validate($request,[
                    'id_role' => 'required',
                ]);
                $validRole = true;
            }else{
                return response()->json([
                    'error' => "La edad es requerida"
                ]); 
            }
            if($validName&&$validEmail&&$validPhoneNumber&&$validAge&&$validPassword&&$validRole){
                $user = User::create([
                    'name'=> addslashes($request->name),
                    'email'=>addslashes($request->email),
                    'phoneNumber'=> addslashes($request->phoneNumber),
                    'age' => addslashes($request->age),
                    'id_role' => addslashes($request->id_role),
                    'password'=>bcrypt($request->password),
                    'urlImage'=>'https://fmpback.ryumicon.com/public/images/users/findmypet-2zfotoGBkG9k1-fotoperfil.svg'
                ]);
                return response()->json([
                    'exito' => "Usuario creado correctamente"
                ]);  
            }else{
                return response()->json([
                      'error' => "Usuario no valido"
                  ]);   
              }
            }
        } catch(Throwable $e) {
            report($e);
                return response()->json([
                    "error" => "A ocurrido un error"
                ]);
        }
    }
}
