<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Models\PetSex;
class PetSexController extends Controller
{
    //
    public function index(){
        try {
            $consulta = PetSex::all();
            return response()->json($consulta);
        } catch (Throwable $e) {
            report($e);
            return response()->json([
                "error" => $e
            ]);

        }
        
    }
    public function obtenerPetSex(){
        try {
            $consulta =DB::table('pet_sexes')->select(
                'id',
                'psName'
            );
            $datos = $consulta->get()->toArray();
            $json = json_decode(json_encode($datos),true);
            return response()->json($json);
        } catch (Throwable $e) {
            report($e);
            return response()->json([
                "error" => $e
            ]);
        }
    }
}
