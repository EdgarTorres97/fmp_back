<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\PetType;
class PetTypeController extends Controller
{
    //
    public function index(){
        try {
            $consulta = PetType::all();
            return response()->json($consulta);
        } catch (Throwable $e) {
            report($e);
            return response()->json([
                "error" => $e
            ]);
        }
    }
    public function obtenerPetType(){
        try {
            $consulta =DB::table('pet_types')->select(
                'id',
                'ptpName'
            );
            $datos = $consulta->get()->toArray();
            $json = json_decode(json_encode($datos),true);
            return response()->json($json);
        } catch (Throwable $e) {
            report($e);
            return response()->json([
                "error" => $e
            ]);
        }
    }
}
