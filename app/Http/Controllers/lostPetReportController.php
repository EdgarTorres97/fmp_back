<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LostPetReport;
use App\Models\PetLocation;
use App\Models\PetImage;
use App\Models\PetType;
use App\Models\PetChip;
use App\Models\PetTag;
use App\Models\PetSex;
use App\Models\PetProcess;
use DB;
use Illuminate\Support\Str;
class lostPetReportController extends Controller
{
    //
    public function crearReporte(Request $request) {
        try {
            $id = auth()->user()->id;
            //$records = json_decode($request, true);
            
            //$model = new LostPetReport($request->all());
            //$model::create();
            //$model = new PetLocation($request->petLocation);
            //$model::create();
            
            $transacResult = DB::transaction(function () use ($request,$id){
                
                $this->validate($request,[
                    'petLocation.id_municipality',
                    'petLocation.plManzana',
                    'petLocation.plRegion',
                    'petLocation.plLote',
                    'petLocation.plPostalCode',
                    'petLocation.plLostDate',
                    'petLocation.pllatitude',
                    'petLocation.pllongitude',
                ]);
                

            
            $petLocation = PetLocation::create([
                'id_municipality'=>addslashes($request->petLocation['id_municipality']),
                'plManzana'=>addslashes($request->petLocation['plManzana']),
                'plRegion'=>addslashes($request->petLocation['plRegion']),
                'plLote'=>addslashes($request->petLocation['plLote']),
                'plPostalCode'=>addslashes($request->petLocation['plPostalCode']),
                'plLostDate'=>addslashes($request->petLocation['plLostDate']),
                'pllatitude'=>addslashes($request->petLocation['plLatitude']),
                'pllongitude'=>addslashes($request->petLocation['plLongitude']),
            ]);

            $this->validate($request,[
                'id_petType',
                'id_petSex',
                'id_petChip',
                'id_petTag',
                'id_petLocation',
                'lprPetName',
                'lprPhoneNumber',
                'lprBreedName',
                'lprSize',
                'lprAge',
                'lprTail',
                'lprEar',
                'lprColor',
                'lprObservation',
            ]);
            
            
            $reporte = LostPetReport::create([
                'id_user'=>addslashes($id),
                'id_petType'=>addslashes($request->id_petType),
                'id_petSex'=>addslashes($request->id_petSex),
                'id_petChip'=>addslashes($request->id_petChip),
                'id_petTag'=>addslashes($request->id_petTag),
                'id_petLocation'=>addslashes($petLocation['id']),
                'lprPetName'=>addslashes($request->lprPetName),
                'lprPhoneNumber'=>addslashes($request->lprPhoneNumber),
                'lprBreedName'=>addslashes($request->lprBreedName),
                'lprSize'=>addslashes($request->lprSize),
                'lprAge'=>addslashes($request->lprAge),
                'lprTail'=>addslashes($request->lprTail),
                'lprEar'=>addslashes($request->lprEar),
                'lprColor'=>addslashes($request->lprColor),
                'lprObservation'=>addslashes($request->lprObservation),
                'id_petProcess'=>1,
            ]);
            PetType::where('id',addslashes($request->id_petType))
            ->increment('ptpCounter', 1);
                
            PetSex::where('id',addslashes($request->id_petSex))
            ->increment('psCounter', 1);
                
            PetChip::where('id',addslashes($request->id_petChip))
            ->increment('pcCounter', 1);
                
            PetTag::where('id',addslashes($request->id_petTag))
            ->increment('ptCounter', 1);
            PetProcess::where('id',1)
            ->increment('ppCounter', 1);
                return $reporte;
            });
            if($transacResult){
                return response()->json([
                    "exito" => "Se ha guardado con exito",
                    "id_lostPetReport" => $transacResult['id']
                ]);
            }else{
                return response()->json([
                    "error" => "Datos no validos"
                ]);
            }
            

            
        } catch (Throwable $e) {
            report($e);
            return response()->json([
                "error" => $e
            ]);

        }

    }
    public function subirImagenReporte(Request $request) {
        try {
            if($request->has('id_lostPetReport')){
                if ($request->hasFile('image'))
                {
                    $id_lostPetReport = $request->input('id_lostPetReport');
                    $file      = $request->file('image');
                    $filename = 'findmypet-';
                    $filename .= Str::random(15);
                    $filename .= '-';
                    $filename  .= $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $picture   = $filename;
                    
                    $file->move(public_path('images/lostPetReport'), $picture);
                    // $url = 'http://localhost:8002/images/lostPetReport/';
                    $url = 'https://fmpback.ryumicon.com/public/images/lostPetReport/';
                    $url .= $filename;
    
                    $reporte = PetImage::create([
                        'id_lostPetReport'=>$id_lostPetReport,
                        'piImageUrl'=>$url,
                    ]);
                    return response()->json(["exito" => "Se ha subido la imagen con exito"]);
                } 
                else
                {
                    return response()->json(["error" => "No se ha podido subir la imagen"]);
                }
            }else{
                return response()->json(["error" => "No se ha podido subir la imagen"]);
            }
            
        } catch (Throwable $e) {
            report($e);
            return response()->json([
                "error" => $e
            ]);

        }
        
    }
    public function misReportes(Request $request) {
        try {
            $id = auth()->user()->id;

            $misreportes =DB::table('users')
            ->join('lost_pet_reports','lost_pet_reports.id_user','=','users.id')
            ->join('pet_types','pet_types.id','=','lost_pet_reports.id_petType')
            ->join('pet_sexes','pet_sexes.id','=','lost_pet_reports.id_petSex')
            ->join('pet_chips','pet_chips.id','=','lost_pet_reports.id_petChip')
            ->join('pet_tags','pet_tags.id','=','lost_pet_reports.id_petTag')
            ->join('pet_processes','pet_processes.id','=','lost_pet_reports.id_petProcess')
            ->join('pet_images','pet_images.id_lostPetReport','=','lost_pet_reports.id')
            ->join('pet_locations','pet_locations.id','=','lost_pet_reports.id_petLocation')
            ->join('municipalities','municipalities.id','=','pet_locations.id_municipality')
            ->select(
                'users.state',
                'lost_pet_reports.id',
                'lost_pet_reports.id_user',
                'lost_pet_reports.id_petType',
                'lost_pet_reports.id_petSex',
                'lost_pet_reports.id_petChip',
                'lost_pet_reports.id_petTag',
                'lost_pet_reports.id_petLocation',
                'lost_pet_reports.lprPetName',
                'lost_pet_reports.lprPhoneNumber',
                'lost_pet_reports.lprBreedName',
                'lost_pet_reports.lprSize',
                'lost_pet_reports.lprAge',
                'lost_pet_reports.lprTail',
                'lost_pet_reports.lprEar',
                'lost_pet_reports.lprColor',
                'lost_pet_reports.lprObservation',
                'lost_pet_reports.id_petProcess',
                'pet_processes.ppName',
                'pet_locations.id_municipality',
                'pet_locations.plManzana',
                'pet_locations.plRegion',
                'pet_locations.plLote',
                'pet_locations.plPostalCode',
                'pet_locations.plLostDate',
                'pet_locations.pllatitude',
                'pet_locations.pllongitude',
                'pet_types.ptpName',
                'pet_chips.pcName',
                'pet_images.piImageUrl',
                'pet_sexes.psName',
                'pet_tags.ptName',

            );

            $misreportes->where('lost_pet_reports.id_user',$id)->where('users.state',true);

            $datos = $misreportes->get()->toArray();
            $json = json_decode(json_encode($datos),true);

            if($json){
                return response()->json($json);
                
            }
            else {
                return response()->json([
                    "error" => "Aún no tienes reportes"
                ]); 

            }
            
            // Validate the value...
        } catch (Throwable $e) {
            report($e);

            return response()->json([
                "error" => $e
            ]);
        }

    }
    public function reportes(Request $request){
        try {
            

            $reportes =DB::table('lost_pet_reports')
            ->join('users','users.id','=','lost_pet_reports.id_user')
            ->join('pet_types','pet_types.id','=','lost_pet_reports.id_petType')
            ->join('pet_sexes','pet_sexes.id','=','lost_pet_reports.id_petSex')
            ->join('pet_chips','pet_chips.id','=','lost_pet_reports.id_petChip')
            ->join('pet_tags','pet_tags.id','=','lost_pet_reports.id_petTag')
            ->join('pet_processes','pet_processes.id','=','lost_pet_reports.id_petProcess')
            ->join('pet_images','pet_images.id_lostPetReport','=','lost_pet_reports.id')
            ->join('pet_locations','pet_locations.id','=','lost_pet_reports.id_petLocation')
            ->join('municipalities','municipalities.id','=','pet_locations.id_municipality')
            ->select(
                
                'lost_pet_reports.id',
                'lost_pet_reports.id_user',
                'lost_pet_reports.id_petType',
                'lost_pet_reports.id_petSex',
                'lost_pet_reports.id_petChip',
                'lost_pet_reports.id_petTag',
                'lost_pet_reports.id_petProcess',
                'pet_processes.ppName',
                'lost_pet_reports.id_petLocation',
                'lost_pet_reports.lprPetName',
                'lost_pet_reports.lprPhoneNumber',
                'lost_pet_reports.lprBreedName',
                'lost_pet_reports.lprSize',
                'lost_pet_reports.lprAge',
                'lost_pet_reports.lprTail',
                'lost_pet_reports.lprEar',
                'lost_pet_reports.lprColor',
                'lost_pet_reports.lprObservation',
                'pet_locations.id_municipality',
                'pet_locations.plManzana',
                'pet_locations.plRegion',
                'pet_locations.plLote',
                'pet_locations.plPostalCode',
                'pet_locations.plLostDate',
                'pet_locations.pllatitude',
                'pet_locations.pllongitude',
                'municipalities.mnName',
                'pet_types.ptpName',
                'pet_chips.pcName',
                'pet_images.piImageUrl',
                'pet_sexes.psName',
                'pet_tags.ptName',

            )->where('users.state',true);
            //$misreportes->where('lost_pet_reports.id_user',$id);
            
            if($request->has('id_petType')) {
                
                if(!$request->input('id_petType')=="") {
                    $reportes->where('lost_pet_reports.id_petType',addslashes($request->input('id_petType')));
                }
                
            } 
            if($request->has('id_petSex')) {
                if(!$request->input('id_petSex')=="") {
                    $reportes->where('lost_pet_reports.id_petSex',addslashes($request->input('id_petSex')));
                }
                
            }
            if($request->has('id_petChip')) {
                if(!$request->input('id_petChip')=="") {
                    $reportes->where('lost_pet_reports.id_petChip',addslashes($request->input('id_petChip')));
                }
                
            }
            if($request->has('id_petTag')) {
                if(!$request->input('id_petTag')=="") {
                    $reportes->where('lost_pet_reports.id_petTag',addslashes($request->input('id_petTag')));
                }  
            }
            
            if($request->petLocation['id_municipality']) {
                
                if(!$request->petLocation['id_municipality']=="") {
                     
                    $reportes->where('pet_locations.id_municipality',addslashes($request->petLocation['id_municipality']));
                }
                
            } 
            if($request->has('id_petProcess')){
                
                if($request->input('id_petProcess')==""||$request->input('id_petProcess')==null){
                    
                    $reportes->where('lost_pet_reports.id_petProcess','2')->orWhere('lost_pet_reports.id_petProcess','3');
                }else{
                    if($request->input('id_petProcess')=="2"||$request->input('id_petProcess')=="3"){
                        $reportes->where('lost_pet_reports.id_petProcess',addslashes($request->input('id_petProcess')));
                    }
                    
                }
            }
            $datos = $reportes->get()->toArray();
            $json = json_decode(json_encode($datos),true);

            if($json){
                return response()->json($json);
            }
            else {
                return response()->json([
                    "error" => "No existen reportes"
                ]); 

            }
        } catch (Throwable $e) {
            report($e);

            return response()->json([
                "error" => $e
            ]);
        }
    }
    public function reportesConfiguracion(Request $request){
        try {
            $isAdmin = auth()->user()->id_role;
            if(strcmp($isAdmin, 1)==0||strcmp($isAdmin, 2)==0){
            
            $reportes =DB::table('lost_pet_reports')
            ->join('users','users.id','=','lost_pet_reports.id_user')
            ->join('pet_types','pet_types.id','=','lost_pet_reports.id_petType')
            ->join('pet_sexes','pet_sexes.id','=','lost_pet_reports.id_petSex')
            ->join('pet_chips','pet_chips.id','=','lost_pet_reports.id_petChip')
            ->join('pet_tags','pet_tags.id','=','lost_pet_reports.id_petTag')
            ->join('pet_processes','pet_processes.id','=','lost_pet_reports.id_petProcess')
            ->join('pet_images','pet_images.id_lostPetReport','=','lost_pet_reports.id')
            ->join('pet_locations','pet_locations.id','=','lost_pet_reports.id_petLocation')
            ->join('municipalities','municipalities.id','=','pet_locations.id_municipality')
            ->select(
                
                'lost_pet_reports.id',
                'lost_pet_reports.id_user',
                'lost_pet_reports.id_petType',
                'lost_pet_reports.id_petSex',
                'lost_pet_reports.id_petChip',
                'lost_pet_reports.id_petTag',
                'lost_pet_reports.id_petProcess',
                'pet_processes.ppName',
                'lost_pet_reports.id_petLocation',
                'lost_pet_reports.lprPetName',
                'lost_pet_reports.lprPhoneNumber',
                'lost_pet_reports.lprBreedName',
                'lost_pet_reports.lprSize',
                'lost_pet_reports.lprAge',
                'lost_pet_reports.lprTail',
                'lost_pet_reports.lprEar',
                'lost_pet_reports.lprColor',
                'lost_pet_reports.lprObservation',
                'pet_locations.id_municipality',
                'pet_locations.plManzana',
                'pet_locations.plRegion',
                'pet_locations.plLote',
                'pet_locations.plPostalCode',
                'pet_locations.plLostDate',
                'pet_locations.pllatitude',
                'pet_locations.pllongitude',
                'municipalities.mnName',
                'pet_types.ptpName',
                'pet_chips.pcName',
                'pet_images.piImageUrl',
                'pet_sexes.psName',
                'pet_tags.ptName',

            )->where('users.state',true);
            //$misreportes->where('lost_pet_reports.id_user',$id);
            
            if($request->has('id_petType')) {
                
                if(!$request->input('id_petType')=="") {
                    $reportes->where('lost_pet_reports.id_petType',$request->input('id_petType'));
                }
                
            } 
            if($request->has('id_petSex')) {
                if(!$request->input('id_petSex')=="") {
                    $reportes->where('lost_pet_reports.id_petSex',$request->input('id_petSex'));
                }
                
            }
            if($request->has('id_petChip')) {
                if(!$request->input('id_petChip')=="") {
                    $reportes->where('lost_pet_reports.id_petChip',$request->input('id_petChip'));
                }
                
            }
            if($request->has('id_petTag')) {
                if(!$request->input('id_petTag')=="") {
                    $reportes->where('lost_pet_reports.id_petTag',addslashes($request->input('id_petTag')));
                }  
            }
            
            if($request->petLocation['id_municipality']) {
                
                if(!$request->petLocation['id_municipality']=="") {
                     
                    $reportes->where('pet_locations.id_municipality',addslashes($request->petLocation['id_municipality']));
                }
                
            } 
            

            if($request->has('id_petProcess')){
                
                if($request->input('id_petProcess')==""){
                    
                    $reportes->where('lost_pet_reports.id_petProcess','1');
                }else{
                    $reportes->where('lost_pet_reports.id_petProcess',addslashes($request->input('id_petProcess')));
                }
            }
            $datos = $reportes->get()->toArray();
            $json = json_decode(json_encode($datos),true);

            if($json){
                return response()->json($json);
            }
            else {
                return response()->json([
                    "error" => "No existen reportes"
                ]); 

            }
        }else {
            return response()->json([
                "error" => "No tienes permiso"
            ]); 

        }

        } catch (Throwable $e) {
            report($e);

            return response()->json([
                "error" => $e
            ]);
        }
    }
    public function lostPetReportId(Request $request){
        try {
            if($request->has('id')){
                $misreportes =DB::table('users')
                ->join('lost_pet_reports','lost_pet_reports.id_user','=','users.id')
                ->join('pet_types','pet_types.id','=','lost_pet_reports.id_petType')
                ->join('pet_sexes','pet_sexes.id','=','lost_pet_reports.id_petSex')
                ->join('pet_chips','pet_chips.id','=','lost_pet_reports.id_petChip')
                ->join('pet_tags','pet_tags.id','=','lost_pet_reports.id_petTag')
                ->join('pet_processes','pet_processes.id','=','lost_pet_reports.id_petProcess')
                ->join('pet_images','pet_images.id_lostPetReport','=','lost_pet_reports.id')
                ->join('pet_locations','pet_locations.id','=','lost_pet_reports.id_petLocation')
                ->join('municipalities','municipalities.id','=','pet_locations.id_municipality')
                ->select(
                    
                    'lost_pet_reports.id',
                    'lost_pet_reports.id_user',
                    'lost_pet_reports.id_petType',
                    'lost_pet_reports.id_petSex',
                    'lost_pet_reports.id_petChip',
                    'lost_pet_reports.id_petTag',
                    'lost_pet_reports.id_petLocation',
                    'lost_pet_reports.lprPetName',
                    'lost_pet_reports.lprPhoneNumber',
                    'lost_pet_reports.lprBreedName',
                    'lost_pet_reports.lprSize',
                    'lost_pet_reports.lprAge',
                    'lost_pet_reports.lprTail',
                    'lost_pet_reports.lprEar',
                    'lost_pet_reports.lprColor',
                    'lost_pet_reports.lprObservation',
                    'lost_pet_reports.id_petProcess',
                    'pet_processes.ppName',
                    'pet_locations.id_municipality',
                    'pet_locations.plManzana',
                    'pet_locations.plRegion',
                    'pet_locations.plLote',
                    'pet_locations.plPostalCode',
                    'pet_locations.plLostDate',
                    'pet_locations.pllatitude',
                    'pet_locations.pllongitude',
                    'municipalities.mnName',
                    'pet_types.ptpName',
                    'pet_chips.pcName',
                    'pet_images.piImageUrl',
                    'pet_sexes.psName',
                    'pet_tags.ptName',

                );

                $misreportes->where('lost_pet_reports.id',addslashes($request->input('id')));

                $datos = $misreportes->get()->toArray();
                $json = json_decode(json_encode($datos),true);

                if($json){
                    return response()->json($json);
                    
                }
                else {
                    return response()->json([
                        "error" => "No existe el reporte"
                    ]); 

                }
            }else {
                return response()->json([
                    "error" => "Datos no validos"
                ]); 

            }
            // Validate the value...
        } catch (Throwable $e) {
            report($e);

            return response()->json([
                "error" => $e
            ]);
        }  
    }
    public function cambiarPetProcess(Request $request){
        $id = auth()->user()->id;
        if($id) {
            try{
                if($request->has('id')&&$request->has('id_petProcess')) {
                    $consulta = DB::table('lost_pet_reports')->where('lost_pet_reports.id',addslashes($request->input('id')));
                    $datos = $consulta->get()->toArray();
                    $json  = json_decode(json_encode($datos),true);
                    if(!$json) {
                        return response()->json([
                            'error' => 'no existen registros'
                        ]);
                    } else {
                        $consulta->update([
                            'id_petProcess' => addslashes($request->input('id_petProcess'))
                        ]);
                        return response()->json([
                            'exito' => 'El proceso se ha actualizado'
                        ]);
                    }
                }else{
                    return response()->json([
                        'error' => 'no existen registros'
                    ]);
                }
            } catch(Throwable $e) {
                return response()->json([
                    'error' => 'Ha ocurrido un error'.$e
                ]);
            }
        } else {
            return response()->json([
                'error' => 'Lo sentimos no puede acceder a esta ruta'
            ]);
        }
    }
}
