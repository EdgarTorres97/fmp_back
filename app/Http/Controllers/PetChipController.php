<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Models\PetChip;
class PetChipController extends Controller
{
    //
    public function index(){
        try {
            $consulta = PetChip::all();
            return response()->json($consulta);
        } catch (Throwable $e) {
            report($e);
            return response()->json([
                "error" => $e
            ]);

        }
        
    }
    public function obtenerPetChip(){
        try {
            $consulta =DB::table('pet_chips')->select(
                'id',
                'pcName'
            );
            $datos = $consulta->get()->toArray();
            $json = json_decode(json_encode($datos),true);
            return response()->json($json);
        } catch (Throwable $e) {
            report($e);
            return response()->json([
                "error" => $e
            ]);
        }
    }
}
