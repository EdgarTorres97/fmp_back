<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PetProcess;
use DB;
class PetProcessController extends Controller
{
    public function index(){
        try {
            $consulta = PetProcess::all();
            return response()->json($consulta);
        } catch (Throwable $e) {
            report($e);
            return response()->json([
                "error" => $e
            ]);
        }
    }
    public function obtenerPetProcesses(){
        try {
            
            $consulta =DB::table('pet_processes')->select(
                'id',
                'ppName',
            );
            $datos = $consulta->get()->toArray();
            $json = json_decode(json_encode($datos),true);
            return response()->json($json);
        } catch (Throwable $e) {
            report($e);
            return response()->json([
                "error" => $e
            ]);
        }
    }
}
