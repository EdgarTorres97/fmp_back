<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Role;
class RoleController extends Controller
{
    //
    public function index(){
        try {
            $consulta = Role::all();
            return response()->json($consulta);
        } catch (Throwable $e) {
            report($e);
            return response()->json([
                "error" => $e
            ]);
        }
    }
    public function obtenerRole(){
        $isAdmin = auth()->user()->id_role;
        try {
                if(strcmp($isAdmin, 1)==0||strcmp($isAdmin, 2)==0) {
                $consulta =DB::table('roles')->select(
                    'id',
                    'roleName',
                    'description'
                );
                $datos = $consulta->get()->toArray();
                $json = json_decode(json_encode($datos),true);
                if($json){
                    return response()->json($json);
                }else{
                    return response()->json([
                        "error" => "No hay roles registrados"
                    ]); 
                }
                
            }else {
                return response()->json([
                    "error" => "No tienes permisos"
                ]); 

            }
        } catch (Throwable $e) {
            report($e);
            return response()->json([
                "error" => $e
            ]);
        }
    }
}
