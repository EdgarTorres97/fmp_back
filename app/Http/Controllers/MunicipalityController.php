<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Models\Municipality;
class MunicipalityController extends Controller
{
    //
    public function index(){
        try {
            $consulta = Municipality::all();
            return response()->json($consulta);
        } catch (Throwable $e) {
            report($e);
            return response()->json([
                "error" => $e
            ]);
        }
    }
    public function obtenerMunicipios(){
        try {
            
            $consulta =DB::table('municipalities')->select(
                'id',
                'mnName',
            );
            $datos = $consulta->get()->toArray();
            $json = json_decode(json_encode($datos),true);
            return response()->json($json);
        } catch (Throwable $e) {
            report($e);
            return response()->json([
                "error" => $e
            ]);
        }
    }
}
