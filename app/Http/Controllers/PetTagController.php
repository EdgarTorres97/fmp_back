<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Models\PetTag;
class PetTagController extends Controller
{
    //
    public function index(){
        try {
            $consulta = PetTag::all();
            return response()->json($consulta);
        } catch (Throwable $e) {
            report($e);
            return response()->json([
                "error" => $e
            ]);
        }
    }
    public function obtenerPetTag(){
        try {
            $consulta =DB::table('pet_tags')->select(
                'id',
                'ptName'
            );
            $datos = $consulta->get()->toArray();
            $json = json_decode(json_encode($datos),true);
            return response()->json($json);
        } catch (Throwable $e) {
            report($e);
            return response()->json([
                "error" => $e
            ]);
        }
    }
}
