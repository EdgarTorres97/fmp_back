<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLostPetReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lost_pet_reports', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_user');
            $table->foreign('id_user')->references('id')->on('users');
            $table->unsignedBigInteger('id_petType');
            $table->foreign('id_petType')->references('id')->on('pet_types');
            $table->unsignedBigInteger('id_petSex');
            $table->foreign('id_petSex')->references('id')->on('pet_sexes');
            $table->unsignedBigInteger('id_petChip');
            $table->foreign('id_petChip')->references('id')->on('pet_chips');
            $table->unsignedBigInteger('id_petTag');
            $table->foreign('id_petTag')->references('id')->on('pet_tags');
            $table->unsignedBigInteger('id_petLocation');
            $table->foreign('id_petLocation')->references('id')->on('pet_locations');
            $table->string('lprPetName')->nullable();
            $table->string('lprPhoneNumber')->nullable();
            $table->string('lprBreedName')->nullable();
            $table->string('lprSize')->nullable();
            $table->string('lprAge')->nullable();
            $table->string('lprTail')->nullable();
            $table->string('lprEar')->nullable();
            $table->string('lprColor')->nullable();
            $table->string('lprObservation')->nullable();
            $table->unsignedBigInteger('id_petProcess');
            $table->foreign('id_petProcess')->references('id')->on('pet_processes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lost_pet_reports');
    }
}
