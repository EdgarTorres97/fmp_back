<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pet_locations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_municipality');
            $table->foreign('id_municipality')->references('id')->on('municipalities');
            $table->integer('plManzana')->nullable();
            $table->integer('plRegion')->nullable();
            $table->integer('plLote')->nullable();
            $table->integer('plPostalCode')->nullable();
            $table->date('plLostDate')->nullable();
            $table->string('plLatitude')->nullable();
            $table->string('plLongitude')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pet_locations');
    }
}
