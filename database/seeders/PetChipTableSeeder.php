<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PetChip;
class PetChipTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->insertPetChip('Si',0);
        $this->insertPetChip('No',0);
    }
    private function insertPetChip($pcName,$pcCounter){
        $petChip = new PetChip();
        $petChip->pcName = $pcName;
        $petChip->pcCounter = $pcCounter;
        $petChip->save();
    }
}
