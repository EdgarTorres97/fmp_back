<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(MunicipalityTableSeeder::class);
        $this->call(PetSexTableSeeder::class);
        $this->call(PetTagTableSeeder::class);
        $this->call(PetChipTableSeeder::class);
        $this->call(PetTypeTableSeeder::class);
        $this->call(PetProcessTableSeeder::class);
    }
}
