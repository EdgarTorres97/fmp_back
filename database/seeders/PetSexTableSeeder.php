<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PetSex;

class PetSexTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->insertPetSex('Macho',0);
        $this->insertPetSex('Hembra',0);
    }
    private function insertPetSex($psName,$psCounter){
        $petSex = new PetSex();
        $petSex->psName = $psName;
        $petSex->psCounter = $psCounter;
        $petSex->save();
    }
}
