<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PetProcess;
class PetProcessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->insertPetProcess('revisión',0);
        $this->insertPetProcess('aceptado',0);
        $this->insertPetProcess('encontrado',0);
    }
    private function insertPetProcess($ppName,$ppCounter){
        $petProcess = new PetProcess();
        $petProcess->ppName = $ppName;
        $petProcess->ppCounter = $ppCounter;
        $petProcess->save();
    }
}
