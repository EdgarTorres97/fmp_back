<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Municipality;
class MunicipalityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->insertMunicipality('Cozumel',0);
        $this->insertMunicipality('Felipe Carrillo Puerto',0);
        $this->insertMunicipality('Isla Mujeres',0);
        $this->insertMunicipality('Othón P. Blanco',0);
        $this->insertMunicipality('Benito Juárez',0);
        $this->insertMunicipality('José María Morelos',0);
        $this->insertMunicipality('Lázaro cárdenas',0);
        $this->insertMunicipality('Solidaridad',0);
        $this->insertMunicipality('Tulum',0);
        $this->insertMunicipality('Bacalar',0);
        $this->insertMunicipality('Puerto Morelos',0);


    }
    private function insertMunicipality($mnName,$mnCounter){
        $municipality = new Municipality();
        $municipality->mnName = $mnName;
        $municipality->mnCounter = $mnCounter;
        $municipality->save();
    }
}
