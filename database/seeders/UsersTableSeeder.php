<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->insertUser('Edgar Antonio Torres Hernández','eath2497@gmail.com','12341234','9984075027',23,'https://fmpback.ryumicon.com/public/images/users/findmypet-2zEdgarGBkG9k1-Edgar.jpg');
        $this->insertUser('Daniela Hernández García','daniela@gmail.com','12341234','9984075027',19,'https://fmpback.ryumicon.com/public/images/users/findmypet-2zfotoGBkG9k1-fotoperfil.svg');
        $this->insertUser('Pedro Emmanuel López Lorenzo','pedro@gmail.com','12341234','9984075027',19,'https://fmpback.ryumicon.com/public/images/users/findmypet-2zfotoGBkG9k1-fotoperfil.svg');
        $this->insertUser('Oscar Arturo Escamilla Solis','oscar@gmail.com','12341234','9984075027',24,'https://fmpback.ryumicon.com/public/images/users/findmypet-2zfotoGBkG9k1-fotoperfil.svg');
    }
    private function insertUser($name,$email,$password,$phoneNumber,$age,$urlImage){
        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->password = bcrypt($password);
        $user->phoneNumber = $phoneNumber;
        $user->age = $age;
        $user->urlImage = $urlImage;
        $user->id_role = 1;
        $user->state = true;
        $user->save();
    }
}
