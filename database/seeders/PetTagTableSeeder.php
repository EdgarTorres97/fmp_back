<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PetTag;

class PetTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->insertPetTag('Si',0);
        $this->insertPetTag('No',0);
    }
    private function insertPetTag($ptName,$ptCounter){
        $petTag = new PetTag();
        $petTag->ptName = $ptName;
        $petTag->ptCounter = $ptCounter;
        $petTag->save();
    }
}
