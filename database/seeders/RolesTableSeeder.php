<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->insertRole('Administrator','Administrador del sistema');
        $this->insertRole('Collaborator','Trabajador del sistema');
        $this->insertRole('User','Usuario del sistema');
    }
    private function insertRole($roleName,$description){
        $role = new Role();
        $role->roleName = $roleName;
        $role->description = $description;
        $role->save();
    }
}
