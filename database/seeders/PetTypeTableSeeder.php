<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PetType;
class PetTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertPetType('Perro',0);
        $this->insertPetType('Gato',0);
    }
    private function insertPetType($ptpName,$ptpCounter){
        $petType = new PetType();
        $petType->ptpName = $ptpName;
        $petType->ptpCounter = $ptpCounter;
        $petType->save();
    }
}
