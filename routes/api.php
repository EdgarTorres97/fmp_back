<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\MunicipalityController;
use App\Http\Controllers\PetChipController;
use App\Http\Controllers\PetSexController;
use App\Http\Controllers\PetTagController;
use App\Http\Controllers\PetTypeController;
use App\Http\Controllers\lostPetReportController;
use App\Http\Controllers\PetProcessController;
use App\Http\Controllers\RoleController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['cors']], function () {
    Route::middleware('auth:api')->group(function () {
        Route::get('/validarToken', [UserController::class, 'validarToken']);
        Route::post('/crearReporte', [lostPetReportController::class, 'crearReporte']);
        Route::post('/subirImagenReporte', [lostPetReportController::class, 'subirImagenReporte']);
        Route::get('/misReportes', [lostPetReportController::class, 'misReportes']);
        
        Route::post('/cambiarPetProcess', [lostPetReportController::class, 'cambiarPetProcess']);
        Route::post('/reportesConfiguracion', [lostPetReportController::class, 'reportesConfiguracion']);
        //Perfil
        Route::post('modificarPerfil', [UserController::class, 'modificarPerfil']);
        Route::get('/petProcesses', [PetProcessController::class, 'obtenerPetProcesses']);
        //usuarios
        Route::get('/usuarios', [UserController::class, 'usuarios']);
        Route::get('/administradores', [UserController::class, 'administradores']);
        Route::get('/colaboradores', [UserController::class, 'colaboradores']);
        Route::post('/usuario', [UserController::class, 'usuario']);
        Route::post('/modificarUsuario', [UserController::class, 'modificarUsuario']);
        Route::post('/estatusUsuario', [UserController::class, 'estatusUsuario']);
        Route::post('/agregarUsuario', [UserController::class, 'agregarUsuario']);
        //roles
        Route::get('/roles', [RoleController::class, 'obtenerRole']);
        

    });
    Route::post('/lostPetReportId', [lostPetReportController::class, 'lostPetReportId']);
    Route::get('/municipios', [MunicipalityController::class, 'obtenerMunicipios']);
    Route::get('/petTypes', [PetTypeController::class, 'obtenerPetType']);
    Route::get('/petChips', [PetChipController::class, 'obtenerPetChip']);
    Route::get('/petSexes', [PetSexController::class, 'obtenerPetSex']);
    Route::get('/petTags', [PetTagController::class, 'obtenerPetTag']);
    Route::post('/reportes', [lostPetReportController::class, 'reportes']);
    Route::post('register', [UserController::class, 'register']);
    Route::post('login', [UserController::class, 'login']);
});